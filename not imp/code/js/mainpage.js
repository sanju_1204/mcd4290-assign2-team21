// Main Page Display
//window.localStorage.clear();
if (localStorage.getItem('routes') !== undefined) {
    showRoutes(getRoutes());
}

//Feature 10: Display all the routes
//-------------------------------------------
function showRoutes(data) {

    if (data.length === 0) {

        let table = document.getElementById("routes_table");
        table.hidden = true;
        let er = document.getElementById("no_routes");
        er.hidden = false;

    } else {
        let table = document.getElementById("routes_table");

        for (let i = 0; i < data.length; i++) {
            let row = table.insertRow(i + 1);
            row.insertCell(0).innerHTML = data[i].name;

            let cell1 = row.insertCell(1)
            cell1.innerHTML = data[i].ship.name;
            cell1.className = "hidden_col";

            let cell2 = row.insertCell(2)
            cell2.innerHTML = data[i].sourcePort.name;
            cell2.className = "hidden_col";

            let cell3 = row.insertCell(3)
            cell3.innerHTML = data[i].destinationPort.name
            cell3.className = "hidden_col";

            let cell4 = row.insertCell(4)
            cell4.innerHTML = data[i].distance;
            cell4.className = "hidden_col";

            let cell5 = row.insertCell(5)
            cell5.innerHTML = data[i].time;
            cell5.className = "hidden_col";

            let cell6 = row.insertCell(6)
            cell6.innerHTML = data[i].cost;
            cell6.className = "hidden_col";

            let cell7 = row.insertCell(7)
            cell7.innerHTML = data[i].startDate;
            cell7.className = "hidden_col";

            row.insertCell(8).innerHTML = ' <input type= "button"' +
                'onclick = "setSelected(' + i + ')" id = "addRouteBtn" value="View Details"/>   '
        }
    }
}

function setSelected(i) {
    localStorage.setItem('selectedRoute', i);
    window.location.replace("viewRoute.html");
}

//get ports from local storage
function getRoutes() {
    let data = JSON.parse(localStorage.getItem('routes'));
    return data;

}
