// Assignment Feature 3: Create Ships

//Function for Validation of Ship Input
function validateShip(){
  let Shipname = document.getElementById('Name').value;
  let Speed = document.getElementById('maxSpeed').value;
  let Range = document.getElementById('range').value;
  let Desc = document.getElementById('desc').value;
  let Cost = document.getElementById('cost').value;
  
  if (!validText(Shipname)){
    document.getElementById("Name").setAttribute("required",true);
  } 
  else if (!validNumber(Speed)){
    document.getElementById("maxSpeed").setAttribute("required",true);
  } 
  else if (!validNumber(Range)){
    document.getElementById("range").setAttribute("required",true);
  } 
  else if (!validText(Desc)) {
    document.getElementById("desc").setAttribute("required",true);
  } 
  else if (!validNumber(Cost)) {
    document.getElementById("cost").setAttribute("required",true);
  } 
  else{
    addNewShip();
  }
}

// Validate Text
function validText(text) {
  return text.length > 2;
}

// Validate Numbers
function validNumber(num) {
  
  if (typeof num == 'number') {
    console.log("number");
    return num > 0;
  } 
  else {
    return false;
  }
}

let userShipArray=[];

// Function for adding the user input to the ship class
function addNewShip()
{
  let ship = new Ship(
    document.getElementById("Name").value,
    document.getElementById("maxSpeed").value,
    document.getElementById("range").value,
    document.getElementById("desc").value,
    document.getElementById("cost").value,)
    
    userShipArray.push(ship);
    storeShips1(userShipArray);
  
  }
  
  // From the viewShip.js
  getShips();
  
  // Local storage for user ships
  
  const SHIP_STORE_KEY1 = "userShip";
  
  function storeShips1(shipList){ 
    let shipSaved = JSON.parse(localStorage.getItem(SHIP_STORE_KEY1));
    
if (shipSaved==null || shipSaved==undefined ){
  shipSaved = [];
}

let newShipList = shipList.concat(shipSaved);
localStorage.setItem(SHIP_STORE_KEY1, JSON.stringify(newShipList));
alert("New Ship added!")

// After the excetution open the viewShip webpage
window.location.replace("viewShips.html");

}


