//Retrieving data from the local storage
let start_port = JSON.parse(localStorage.getItem("start_port"))
let dest_port = JSON.parse(localStorage.getItem("dest_port"))
let choosenShip = JSON.parse(localStorage.getItem("choosenShip"))
let RouteSaved = JSON.parse(localStorage.getItem("routeSaved"))

//Creating a new time instance
let time_departure = new Date();

function retrieveJourney() {
    let journeyDiv = document.getElementById("myJourney");
    console.log(choosenShip);
    let routeChosen = JSON.parse(localStorage.getItem("routeChosed"));
    //error checking for ship range
    let display = ""
    if (choosenShip._range < parseInt(routeChosen._distance)) {
        display = "Ship is not suitable for the journey";
        alert("Ship range is less than the route distance please select another ship with higher range")
    } else {

        display += "<h4><b>" + "Ship Information" + "</b></h4>" + "<br>"
        display += "Selcted Ship name: " + routeChosen._ship + "<br>"
        display += "Port distance: " + routeChosen._distance +" km"+ "<br>"
        display += "Total Cost: "  + " $" +routeChosen._cost + "<br>"
        display += "Duration: " + (routeChosen._time).toFixed(2) +" hrs"+ "<br>"
        display += "<h4><b>" + "Starting Port" + "</b></h4>" + "<br>" +
            "Name: " + routeChosen._sourcePort + "<br>" + "<br>" +
            "<h4><b>" + "Destination Port" + "</b></h4>" + "<br>" +
            "Name: " + routeChosen._destinationPort + "<br>"



    }
    journeyDiv.innerHTML = display
}
retrieveJourney()

//postponing the day of departure
function Postpone() {

    console.log("inside postpone")
    time_departure = hours_add(time_departure, 24)
    retrieveJourney()
    alert("Your Journey has been postponed by one day, if you wish to postpone it again please press the postpone button once more ")
    // add 1 day to date of departure/arrival
}

function hours_add(d, hr_add) {
    var dt = new Date(d);
    let a = hr_add.toFixed()
    dt.setHours(d.getHours() + Number(a));
    return dt

}

function DeleteRoute(){
    let journeyDiv = document.getElementById("myJourney")
    journeyDiv.innerHTML=""
    alert("Deleted")

}