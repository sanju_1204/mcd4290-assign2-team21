//Feature 4: View Ships


function showShips(Shipdata,table_id) {
    var table = document.getElementById(table_id);

    for (let i = 0; i < Shipdata.length; i++) {
        var row = table.insertRow(i + 1);
        row.insertCell(0).innerHTML = Shipdata[i].name;
        row.insertCell(1).innerHTML = Shipdata[i].maxSpeed;
        row.insertCell(2).innerHTML = Shipdata[i].range;
        row.insertCell(3).innerHTML = Shipdata[i].desc;
        row.insertCell(4).innerHTML = Shipdata[i].cost;
        row.insertCell(5).innerHTML = Shipdata[i].status;
    }

}

 
let s_List = new ShipList(); 
let UserShipdata=s_List.getUserShips();
let ApiShipdata=s_List.getApiShips();

showShips(UserShipdata, "UserShip_table")
showShips(ApiShipdata, "ApiShip_table") 









