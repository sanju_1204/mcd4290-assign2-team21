class Ship
{
  constructor(shipName,maxSpeed,range,desc,cost)
  {
    this._shipName=shipName;
    this._maxSpeed=maxSpeed;
    this._range=range;
    this._desc=desc;
    this._cost=cost;
    this._status="availaible";
    
  }
  get shipName(){return this._shipName;}
  get maxSpeed(){return this._maxSpeed;}
  get range(){return this._range;}
  get desc(){return this._desc;}
  get cost(){return this._cost;}
  get status(){return this._status;}
  get comments(){return this._comments;}

  set shipName(newShip){this._shipName=newShip;}
  set maxSpeed(newSpeed){this._maxSpeed=newSpeed;}
  set range(newRange){this._range=newRange;}
  set desc(newDesc){this._desc=newDesc;}
  set cost(newCost){this._cost=newCost;}
  set status(newStatus){this._status=newStatus;}
  
  
  printShip()
  {
  let output1 = "";

  output1 += "Ship Name: " + document.getElementById("PortName").value;
  output1 += "Maximum Speed: " + document.getElementById("PortCountry").value;
  output1 += "Range: " + document.getElementById("PortType").value;
  output1 += "Port size: " + document.getElementById("PortSize").value;
  output1 += "Description: " + document.getElementById("PortPrecision").value;
  output1 += "Cost: " + document.getElementById("PortLat").value;
  output1 += "Status: " + "availaible";

  return output1
  }


}


//  Class for Port
class Port
{
    constructor(name,country,type,size,precision,latitude,longitude, location)
    {
        this._name = name;
        this._country = country;
        this._type = type;
        this._size = size;
        this._precision = precision;
        this._latitude = latitude;
        this._longitude = longitude;
       this._location = location;
    }

    get name(){return this._name;}
    get country(){return this._country;}
    get type(){return this._type;}
    get size(){return this._size;}
    get precision(){return this._precision;}
    get latitude(){return this._latitude;}
    get longitude(){return this._longitude;}
    get location(){return this.location;}

    set name(newPort){this._name = newPort;}
    set country(newCountry){this._country = newCountry;}
    set type(newType){this._type = newType;}
    set size(newSize){this._size = newSize;}
    set precision(newPrecision){this._precision = newPrecision;}
    set latitude(newLatitude){this._latitude = newLatitude;}
    set longitude(newLongitude){this._longitude = newLongitude;}
    set location(newLocation){this._location = newLocation;}


}




class Route {
    constructor( ship, sourcePort, destinationPort, distance, time, cost, startDate) {


        this._ship = ship;
        this._sourcePort = sourcePort;
        this._destinationPort = destinationPort;
        this._distance = distance;
        this._time = time;
        this._cost = cost;
        this._startDate = startDate

    }
    //accessors

    get ship() {
        return this._ship;
    }
    get sourcePort() {
        return this._sourcePort;
    }
    get destinationPort() {
        return this._destinationPort;
    }
    get distance() {
        return this._distance;
    }
    get time() {
        return this._time;
    }
    get cost() {
        return this._cost;
    }
    get startDate() {
        return this._startDate;
    }


    //mutators
    set name(newName) {
        this._name = newName;
    }
    set time(newTime) {
        this._time = newtime;
    }

    //methods
    //More code here
    toString() {
        return this._name + " " + this._country + this._
    }
}
class ShipList {
    constructor(ships) {
        this.ships = ships;

    }
}

class PortList {
    constructor(ports) {
        this.ports = ports;

    }
}

class routeList{
    constructor(){
        this._routes = [];
        let Route = new route();
        this._routes.push(Route);
    }
}