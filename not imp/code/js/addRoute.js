//ShipSelection
/*
    let data1 = JSON.parse(localStorage.getItem('ships'));
    let optionRef1 = document.getElementById("ships")
    let option1 = "<option></option>"
    let shipNames = []
    //Pushing name into a new empty array 
     for (let push1 = 0; push1 < data1.length; push1++) {
     shipNames.push(data1[push1].name)     
     }
    shipNames.sort()
    console.log(shipNames)

    for (let i = 0; i < shipNames.length; i++){
    option1+= "<option>"+ shipNames[i] + "</option>";
    }

   optionRef1.innerHTML=option1
    JSON.stringify(data1)
*/

//Original Port Selection
    
    let data2 = JSON.parse(localStorage.getItem('ports'));
    let optionRef2 = document.getElementById("originPorts")
    let option2 = "<option></option>"
    let originPortsNames = []
    //Pushing name into a new empty array 
     for (let push2 = 0; push2 < data2.length; push2++) {
     originPortsNames.push(data2[push2].name)     
     }
    originPortsNames.sort()
    console.log(originPortsNames)

     for (let j = 0; j < originPortsNames.length; j++) {
     option2+= "<option>" + originPortsNames[j] + "</option>";
     }

    optionRef2.innerHTML=option2 
    JSON.stringify(data2)


//Destination Port Selection
    
    let data3 = JSON.parse(localStorage.getItem('ports'));
    let optionRef3 = document.getElementById("destPorts")
    let option3 = "<option></option>"
    let destPortsNames = []
    //Pushing name into a new empty array 
     for (let push3 = 0; push3 < data3.length; push3++) {
     destPortsNames.push(data3[push3].name)     
     }
    destPortsNames.sort()

     for (let k = 0; k < destPortsNames.length; k++) {
     option3+= "<option>" + destPortsNames[k] + "</option>";
          
     }

    optionRef3.innerHTML=option3
    JSON.stringify(data2)

    mapboxgl.accessToken = 'pk.eyJ1Ijoic2FuamF5LTEyMDQiLCJhIjoiY2tjdmM3YmQ3MDJ0cTJ4cW9oYm1zZnkzaCJ9.YYbeeiq6lnhpkBEHTm4YzQ';//insert API Key

    //initialise map
    map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: [145.133957,-37.907803], // starting position [lng, lat]
    zoom: 5 // starting zoom
    });  

let routes = [];
//display country
var distanceSel=0

function displayCountry()
{
  
    var originPortsSel=document.getElementById("originPorts").value
    console.log(originPortsSel)
    var destPortsSel=document.getElementById("destPorts").value
    console.log(destPortsSel)
    var startDateSel=document.getElementById("date").value
    console.log(startDateSel)
    var nameSel=document.getElementById("name").value
    console.log(nameSel)
    
   
    if(originPortsSel){
    //Find the position of originPortsSel that the user selected in the array   
    const foundOriginPorts = data2.find(({ name }) => name === originPortsSel)
    console.log(foundOriginPorts)
    //Show country
    var originCountry =foundOriginPorts.country
    console.log(originCountry)
    var showOriginCountry=document.getElementById("OriginCountry")
    showOriginCountry.innerHTML=originCountry
    //Get the Longtitude from originPorts
    var OriginLongtitude = foundOriginPorts.lng
    console.log(OriginLongtitude)
    //Convert the Longtitude into the radian 
    var OriginLongtitudeCal=OriginLongtitude/(180/Math.PI)
    console.log(OriginLongtitudeCal)
    //Get the Latitude from originPorts
    var OriginLatitude = foundOriginPorts.lat
    console.log(OriginLatitude)
    //Convert the Latitude into the radian
    var OriginLatitudeCal=OriginLatitude/(180/Math.PI)
    console.log(OriginLatitudeCal)
    }
    
    if(destPortsSel){
    //Find the position of DestPortsSel that the user selected in the array  
    const foundDestPorts = data2.find(({ name }) => name === destPortsSel)
    console.log(foundDestPorts)
    //Show country
    var DestCountry =foundDestPorts.country
    console.log(originCountry)
    var showDestCountry=document.getElementById("DestCountry")
    showDestCountry.innerHTML=DestCountry
    //Get the Longtitude from DestPortsSel
    var DestLongtitude = foundDestPorts.lng
    console.log(DestLongtitude)
    //Convert the Longtitude into the radian  
    var DestLongtitudeCal=DestLongtitude/(180/Math.PI)
    console.log(DestLongtitudeCal)
    //Get the Latitude from DestPortsSel
    var DestLatitude = foundDestPorts.lat
    console.log(DestLatitude)
    //Get the Latitude from DestPortsSel
    var DestLatitudeCal=DestLatitude/(180/Math.PI)
    console.log(DestLatitudeCal)
    }
    //Distance
    //Formula of calculating distance 
    //Distance, d = 3963.0 * arccos[(sin(lat1) * sin(lat2)) + cos(lat1) * cos(lat2) * cos(long2 – long1)]
    //Step 1:(sin(lat1) * sin(lat2))
    var Step1 =Math.sin(OriginLatitudeCal)*Math.sin(DestLatitudeCal)
    console.log(Step1)
    //Step 2:cos(lat1) * cos(lat2)
    var Step2 = Math.cos(OriginLatitudeCal)*Math.cos(DestLatitudeCal)
    console.log(Step2)
    //Step 3:long2 – long1
    var Step3 = DestLongtitudeCal-OriginLongtitudeCal
    console.log(Step3)
    //Step 4: cos(long2 – long1)
    var Step4 = Math.cos(Step3)
    console.log(Step4)
    //Step 5: arccos[(sin(lat1) * sin(lat2)) + cos(lat1) * cos(lat2) * cos(long2 – long1)]
    var Step5 = Math.acos(Step1+Step2*Step4)  
    console.log(Step5)
    //Distance calculated: 3963.0 * arccos[(sin(lat1) * sin(lat2)) + cos(lat1) * cos(lat2) * cos(long2 – long1)]
    distanceSel =  (3963.0 * Step5).toFixed(2)
    console.log(distanceSel)
   var showDistance=document.getElementById("Distance")
   showDistance.innerHTML="Distance:" + distanceSel + "Km"
    
    /* Change calculation of distanceSel, timeSel, costSel
    
     if(wayPointListSel){
     //Longtitude of the wayPoint 
     //Latitude of the wayPoint 
     }else{
      var wayPointListSel=0 
     }
   */ 
    
    //Store in local Storage
   
    /* 
    var RouteDetail = new Route (nameSel, shipSel, sourcePortSel, destinationPortSel, distanceSel, timeSel, costSel, startDateSel, wayPointListSel)
    routes.push(RouteDetail);
    rList = new RouteList();
    rList.addRoute(RouteDetail);
    */
    

    var originCoords = [OriginLongtitude,OriginLatitude]
    var destCoords = [DestLongtitude,DestLatitude]
    
    map.panTo([DestLongtitude,DestLatitude], {duration: 300});
    
    //display green marker for source port
   var originPortMarker = new mapboxgl.Marker({
                color: "#00ff00" })
            .setLngLat(originCoords)
            .addTo(map);
    
    //display red marker for destination port
    var destPortMarker = new mapboxgl.Marker({
                color: "#ff0000" })
            .setLngLat(destCoords)
            .addTo(map);
    
    //display waypoints

        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [
                        [OriginLongtitude,OriginLatitude],
                        [DestLongtitude,DestLatitude],
                    ]
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#FFA500',
                'line-width': 3
            }
        });
    
    let newPoints
    let newPointsLng
    let newPointsLat
    let newPointsArray = []
    let newPointsLngArray = []
    let newPointsLatArray = []
    
    map.on('click', function(e) {
        newPoints = e.lngLat
        newPointsArray.push(newPoints)

        newPointsLng = newPoints.lng
        newPointsLat = newPoints.lat
        newPointsLngArray.push(newPointsLng)
        newPointsLatArray.push(newPointsLat)

        
            var newPointsMarker = new mapboxgl.Marker()
            .setLngLat([newPointsLng,newPointsLat])
            .addTo(map); 
        
    });
    
    
    // Used to draw a line between points
    var linestring = {
        'type': 'Feature',
        'geometry': {
            'type': 'LineString',
            'coordinates': []
        }
    };
    
                // If a feature was clicked, remove it from the map
            if (features.length) {
                var id = features[0].properties.id;
                geojson.features = geojson.features.filter(function (point) {
                    return point.properties.id !== id;
                });
            } else {
                var point = {
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [e.lngLat.lng, e.lngLat.lat]
                    },
                    'properties': {
                        'id': String(new Date().getTime())
                    }
                };

                geojson.features.push(point);
            }

            if (geojson.features.length > 1) {
                linestring.geometry.coordinates = geojson.features.map(
                    function (point) {
                        return point.geometry.coordinates;
                    }
                );

                geojson.features.push(linestring);}
                
                
        map.on('mousemove', function (e) {
        //document.getElementById('info').innerHTML =
            // e.point is the x, y coordinates of the mousemove event relative
            // to the top-left corner of the map
            //JSON.stringify(e.point) +
            //'<br />' +
            // e.lngLat is the longitude, latitude geographical position of the event
           // JSON.stringify(e.lngLat.wrap());
            console.log(e.lngLat)
    }); 

var rangeSel
function shipSelection(){
   
    var shipSel=document.getElementById("ships").value
    console.log(shipSel) 
   if(shipSel){
    //Find the position of shipSel that the user selected in the array 
    const foundShip = data1.find(({ name }) => name === shipSel)
    console.log(foundShip)
    //Pick the cost from the obj
    var costSelKm=foundShip.cost
    console.log(costSelKm)
    //Pick the speed from the obj
    var speedSel = foundShip.maxSpeed 
    console.log(speedSel)
    //Get the range from the obj
    rangeSel = foundShip.range
    console.log(rangeSel)
    //timeSel
    var timeSel = (distanceSel/speedSel).toFixed(2) 
    console.log(timeSel)
    //costSel 
    var costSel = (distanceSel*costSelKm).toFixed(2)
    console.log(costSel)
   } 
    
   var showTime=document.getElementById("shipTime")
   showTime.innerHTML="Time : " + timeSel + "  Hours"
   var showCost=document.getElementById("shipCost")
   showCost.innerHTML="Cost: $ " + costSel
    
   }
function Elimination(){
   
let data1 = JSON.parse(localStorage.getItem('ships'));
    let optionRef1 = document.getElementById("ships")
    let option1 = "<option></option>"
    let shipNames = []
    //Pushing name into a new empty array 
     for (let push1 = 0; push1 < data1.length; push1++) {
         if(data1[push1].range > distanceSel){
             shipNames.push(data1[push1].name)
         }  
     }
    shipNames.sort()
    console.log(shipNames)

    for (let i = 0; i < shipNames.length; i++){
    option1+= "<option>"+ shipNames[i] + "</option>";
    }

   optionRef1.innerHTML=option1
    JSON.stringify(data1)
 
} }