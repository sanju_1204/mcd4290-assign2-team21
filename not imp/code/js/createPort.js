let portLocation;

// Feature 5: Create Ports
//----------------------------------------------
function addPort() {

    //get location from OpenCage Api
    if (portLocation === undefined) {
        //Return error to the page
        alert("The location not found so port was not created.");
        return;
    }
    let p = new Port(document.getElementById('portName').value,
        document.getElementById('country').value,
        document.getElementById('portType').value,
        document.getElementById('portSize').value,
        document.getElementById('precision').value,
        // data.formatted,
        portLocation.geometry.lat,//this should be lat
        portLocation.geometry.lng,
    );


    let pList = new PortList();
    pList.addPort(p);

    alert("Port created successfully");
    window.location.replace("viewPorts.html");

}


function getLocation() {
    //validate form here

    let loc = document.getElementById('location').value;
    let url = 'https://api.opencagedata.com/geocode/v1/json?q='
        + loc + '&key=daaa31acf84844739deadd9ffcfc87b4&language=en&pretty=1';
    let http = new XMLHttpRequest();
    http.open("GET", url);
    http.send();
    http.onreadystatechange = function () {

        if (http.status === 200 && http.readyState === 4) {
            let response = http.responseText;
            portLocation = JSON.parse(response).results[0];
            console.log("CHAnged" + portLocation);
            console.log("LOC " + portLocation);

            addPort();

        } else {
            //show error
            console.log("Error" + http.readyState);
        }

    }

}


