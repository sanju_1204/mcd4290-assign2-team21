//Feature 1: Create Ship, Port & Route classes

// Ship Class 
class Ship { 
    constructor(name, maxSpeed, range, desc, cost, status) { 
        this.name = name; 
        this.maxSpeed = maxSpeed; 
        this.range = range; 
        this.desc = desc; 
        this.cost = cost; 
        this.status = status; 
    } 
    
  
} 
 
//Shiplist Class 
class ShipList { 
    constructor() { 
        this.userShiplist = [] 
        this.Apishiplist = [] 
    } 
 
    // Load Ships From API 
    loadShipsFromApi() { 
        
        let url = "https://eng1003.monash/api/v1/ships/"; 
        let http = new XMLHttpRequest(); 
 
        http.open("GET", url); 
        http.send(); 
        http.onreadystatechange = function () { 
 
            // console.log("Status " + http.status + " State " + http.readyState); 
 
            if (http.status === 200 && http.readyState === 4) { 
                // set data to table 
                let response = http.responseText; 
 
                let data = JSON.parse(response).ships; 
                localStorage.setItem('ships', JSON.stringify(data)); 

            } 
 
        } 
    } 


    loadApiShips() { 
        if (localStorage.getItem('ships') !== undefined) { 
            let ships = JSON.parse(localStorage.getItem('ships')); 
            if (ships === null) { 
                this.loadShipsFromApi(); 
                this.Apishiplist = JSON.parse(localStorage.getItem('ships')); 
            } else { 
                this.Apishiplist = ships; 
            } 
        }
 
    } 

    getApiShips() { 
        this.loadApiShips(); 
        return this.Apishiplist; 
    } 

    loadUserShip() { 
        if (localStorage.getItem('user_ships') !== undefined) { 
            let user_ships = JSON.parse(localStorage.getItem('user_ships')); 
 
            if (user_ships !== null) { 
                this.userShiplist = user_ships; 
            } 
        } 
    } 

    getUserShips() { 
 
        this.loadUserShip(); 
        return this.userShiplist; 
    } 
 

    // Add Ship 
    addShip(ship) { 
        this.loadUserShip(); 
        this.userShiplist.push(ship); 
        // Save Local 
        localStorage.setItem('user_ships', JSON.stringify(this.userShiplist)); 
    } 
 
 
} 
 
 
//Port Class 
class Port { 
    constructor(name, country, type, size, locprecision,lat, lng) { 
        this.name = name; 
        this.country = country; 
        this.type = type; 
        this.size = size; 
        this.locprecision = locprecision; 
        this.lat = lat; 
        this.lng = lng; 
 
    } 
} 
 
// Port List Class 
class PortList { 
    constructor() { 
        this.userPortlist = [] 
        this.ApiPortlist = [] 
    } 
 
    loadUserPorts() { 
        if (localStorage.getItem('user_ports') !== undefined) { 
            let user_ports = JSON.parse(localStorage.getItem('user_ports')); 
 
            if (user_ports !== null) { 
                this.userPortlist = user_ports; 
            } 
        } 
    } 
 
    loadApiPorts() { 
        if (localStorage.getItem('ports') !== undefined) { 
            let ports = JSON.parse(localStorage.getItem('ports')); 
            if (ports === null) { 
                this.loadPortsFromApi(); 
                this.ApiPortlist = JSON.parse(localStorage.getItem('ports')); 
            } else { 
                this.ApiPortlist = ports; 
            } 
 
        } else { 
            this.loadPortsFromApi(); 
            this.ApiPortlist = JSON.parse(localStorage.getItem('ports')); 
 
        } 
 
    } 
 
    getUserPorts() { 
 
        this.loadUserPorts(); 
        return this.userPortlist; 
    } 
 
    getApiPorts() { 
        this.loadApiPorts(); 
        return this.ApiPortlist; 
    } 
 
    getAllPorts() { 
        if (localStorage.getItem('ports') !== undefined) { 
            let ports = JSON.parse(localStorage.getItem('ports')); 
 
            if (ports === null) { 
                this.loadPortsFromApi(); 
            } 
            this.loadUserPorts(); 
            for (let n = 0; n < this.userPortlist.length; n++) { 
                ports.push(this.userPortlist[n]) 
            } 
            return ports; 
 
        } else { 
            this.loadPortsFromApi(); 
            let port_ = JSON.parse(localStorage.getItem('ports')); 
            this.loadUserPorts(); 
            for (let n = 0; n < this.userPortlist.length; n++) { 
                port_.push(this.userPortlist[n]) 
            } 
            return port_; 
        } 
    } 
 
    // Load Ports From API 
    loadPortsFromApi() { 
 
        let url = "https://eng1003.monash/api/v1/ports/"; 
        let http = new XMLHttpRequest(); 
 
        http.open("GET", url); 
        http.send(); 
        http.onreadystatechange = function () { 
 
            // console.log("Status " + http.status + " State " + http.readyState); 
 
            if (http.status === 200 && http.readyState === 4) { 
                let response = http.responseText; 
                let data = JSON.parse(response).ports; 
                //save to local storage 
                localStorage.setItem('ports', JSON.stringify(data)); 
 
            } 
        } 
    } 
 
    // Add Port 
    addPort(port) { 
        this.loadUserPorts(); 
        this.userPortlist.push(port); 
        // Save Local 
        localStorage.setItem('user_ports', JSON.stringify(this.userPortlist)); 
    } 
 
 
} 

let mapboxkey = 'pk.eyJ1IjoiZW1tYW51ZWxrIiwiYSI6ImNrZGx1cWNyZTA2bWozM3FuNHFqZXhmcWkifQ.psvHYw-JHbA48L0W7_EygQ'; 

//Route Class 
class Route { 
    constructor(name, ship, sourcePort, destinationPort, distance, time, cost, 
                startDate, wayPointList) { 
        this.name = name; 
        this.ship = ship; 
        this.sourcePort = sourcePort; 
        this.destinationPort = destinationPort; 
        this.distance = distance; 
        this.time = time; 
        this.cost = cost; 
        this.startDate = startDate; 
        this.wayPointList = wayPointList; 
 
    } 
} 
 
class RouteList { 
    constructor() { 
        this.routes = [] 
    } 
 
    loadRoutes() { 
        if (localStorage.getItem('routes') !== undefined) { 
            let user_ships = JSON.parse(localStorage.getItem('routes')); 
 
            if (user_ships !== null) { 
                this.routes = user_ships; 
            } 
        } 
    } 
 
    getRoutes() { 
        return this.routes; 
    } 
 
 
    // Add Route 
    addRoute(rote) { 
        this.loadRoutes(); 
        this.routes.push(rote); 
        // Save Local 
        localStorage.setItem('routes', JSON.stringify(this.routes)); 
    } 
 
 
} 
 
