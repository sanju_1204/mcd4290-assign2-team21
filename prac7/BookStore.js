let listOfAllKnownAuthors = []

// Bookstore class defines the data of the books inside the store
class BookStore
{
    constructor(name, address, owner)
    {   
        // declare private attributes :
        this._name = name;
        this._address = address;
        this._owner = owner;
        this._booksAvailable = [];
        this._totalCopiesOfAllBooks = 0
    }
    
    // Public methods : 

    authorKnown(authorName)
    {
        let foundThem = false;
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])
            {
                foundThem = true
            }
        }
        return foundThem
    }

    // Book coming in to the store
    addBook(bookInstance, copies)
    {
        let positionOfBook = this.checkForBook(bookInstance);
        if (positionOfBook != null)
        {
             let foundBook = this._booksAvailable[positionOfBook];
             foundBook.copies += copies;
             console.log("Added " + copies + " copies of " + foundBook.book);
             listOfAllKnownAuthors.push(foundBook.book.author);
        }
        else
        {
             let bookCopies = {
                 book: bookInstance,
                 copies: copies
             };
             this._booksAvailable.push(bookCopies);
             console.log("Added " + copies + " copies of a new book: " + bookInstance);
        }

        this._totalCopiesOfAllBooks += copies;
    }

    //checking how many copies of specific books are sold
    sellBook(bookInstance, numberSold)
    {
        let positionOfBook = this.checkForBook(bookInstance);
        if (positionOfBook != null)
        {   
            let foundBook = this._booksAvailable[positionOfBook];
            if (numberSold > this._booksAvailable[positionOfBook].copies)
            {
                console.log("Not enough copies of " + foundBook.book + " to sell");
            }
            else
            {
                foundBook.copies -= numberSold;
                if (foundBook.copies === 0)
                {
                    this._booksAvailable.pop(PositionOfBook);
                    this._NumTitles -= 1;
                    let foundAuth = this.authorKnown(foundBook.book.author);
                    listOfAllKnownAuthors.pop(foundAuth);
                }
                this._totalCopiesOfAllBooks -= numberSold;
                console.log("Sold " + numberSold + " copies of " + foundBook.book);
            }
        }
        else
        {
            console.log(bookInstance + " not found");
        }
    }

    checkForBook(bookInstance)
    {
        let currBookNum = 0;
        while (currBookNum < this._booksAvailable.length)
        {
            if (this._booksAvailable[currBookNum].book.isTheSame(bookInstance))
            {
                return currBookNum;
            }
            else
            {
                return null;
            }
            currBookNum += 1;
        }
        return null;
    }
//Getter of Name 
    get name()
    {
        return this._name;
    }
//Setter of Name
    set name(newName)
    {
        this._name = newName;
    }
//Getter of Address 
    get address()
    {
        return this._address;
    }
//Setter of Address
    set address(newAddress)
    {
        this._address = newAddress;
    }
//Getter of Owner 
    get owner()
    {
        return this._owner;
    }
//Setter of Owner
    set address(newOwner) // the naming of this setter is not clear, confusing  
    {
        this._owner = newOwner;
    }
}

class Book // classing book 
{
    constructor(title, author, publicationYear, price) /
    {
        this._title = title;
        this._author = author;
        this._publicationYear = publicationYear;
        this._price = price;
        if (this.authorKnown(this._author) === false) // using if statement + giving a condition as a boolean
        {
            listOfAllKnownAuthors.push(this._author)
        }
    }

    isTheSame(otherBook)
    {
        return otherBook.price === this.price;
    }

    authorKnown(authorName)
    {
        // defining new variable founfThem
        let foundThem = false;
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])  
            {
                foundThem = true;     
            }
        }
        // returing the value of foundThem
        return foundThem;
    }
// getting the value of title
    get title()
    {
        // returning the value of title
        return this._title;
    }
// getting the value of author
    get author()
    {
        // returning the value of author
        return this._author;
    }
// getting the value of publicationYear
    get publicationYear()
    {
        // returning the value of publicationYear
        return this._publicationYear;
    }
// getting the value of price
    get price()
    {
        // returning the value of price
        return this._price;
    }

    toString()
    {
        return this.title + ", " + this.author + ". " + this.publicationYear + " ($" + this.price + ")";
    }
}

// Book details courtesy of Harry Potter series by J.K. Rowling
// defining new local variable cheapSpellBook
let cheapSpellBook = new Book("The idiot's guide to spells","Morlan",2005,40);
// defining new local variable flourishAndBlotts 
let flourishAndBlotts = new BookStore("Flourish & Blotts", "North side, Diagon Alley, London, England", "unknown");
// defining new local variable monsterBook
let monsterBook = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
// defining new local variable monsterBookToSell
let monsterBookToSell = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
// defining new local variable spellBook 
let spellBook = new Book("The Standard Book of Spells, Grade 4", "Miranda Goshawk", 1921, 80);

flourishAndBlotts.addBook(cheapSpellBook,1000);
flourishAndBlotts.addBook(monsterBook, 500);
flourishAndBlotts.sellBook(monsterBookToSell, 200);
flourishAndBlotts.addBook(spellBook, 40);
flourishAndBlotts.addBook(spellBook, 20);
flourishAndBlotts.sellBook(spellBook, 15);
flourishAndBlotts.addBook(monsterBookToSell, -30);
flourishAndBlotts.sellBook(monsterBookToSell, 750);

// displaying the values of listOfAllKnownAuthors in console
console.log("Authors known: " + listOfAllKnownAuthors);
