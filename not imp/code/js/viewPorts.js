//Feature 6: View Ports
//---------------------------------------


//user_ports_table
//display data on table API Ports
//window.localStorage.clear();
function showPorts(Portdata, table_id) {
    let Port_table = document.getElementById(table_id);
    for (let i = 0; i < Portdata.length; i++) {
        let row = Port_table.insertRow(i + 1);
        row.insertCell(0).innerHTML = Portdata[i].name;
        row.insertCell(1).innerHTML = Portdata[i].country;
        row.insertCell(2).innerHTML = Portdata[i].type;
        row.insertCell(3).innerHTML = Portdata[i].size;
        row.insertCell(4).innerHTML = Portdata[i].locprecision;
        row.insertCell(5).innerHTML = "(" + Portdata[i].lat + "," + Portdata[i].lng + ")";
    }
}

let portList = new PortList();
let UserPortdata=portList.getUserPorts();
let ApiPortdata=portList.getApiPorts();



showPorts(ApiPortdata, "ApiPort_table")

showPorts(UserPortdata, "UserPort_table")





