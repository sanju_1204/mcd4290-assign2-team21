mapboxgl.accessToken = "pk.eyJ1IjoieW9nLTQ2IiwiYSI6ImNrY3d2dTJicDBpM3IycHFmZWIycWhwNnMifQ.K1S4ujE6UJH42HlCGCPrjQ";

let map = new mapboxgl.Map({
    container: 'map',
    center: [77.5946,12.9716],
    zoom: 1.5,
    style: 'mapbox://styles/mapbox/streets-v9'
});
let deplatitude = 0;
let deplongitude = 0;
let depLocation = [];

var distanceContainer = document.getElementById("distance");

var geojson = {
    "type": "FeatureCollection",
    "features": []
};

var linestring = {
    "type": "Feature",
    "geometry": {
        "type": "LineString",
        "coordinates": []
    }
};

map.on('load', function() {
    map.addSource('geojson', {
        "type": "geojson",
        "data": geojson
    });

    map.addLayer({
        id: 'linestring',
        type: 'line',
        source: 'geojson',
        layout: {
            'line-cap': 'round',
            'line-join': 'round'
        },
        paint: {
            'line-color': '#000',
            'line-width': 2.5
        },
        filter: ['in', '$type', 'LineString']
    });
    
    distanceContainer.innerHTML = '';
    
    var value = document.createElement('pre');
        value.textContent =
            'Total distance: ' + turf.length(linestring).toLocaleString() + 'km';
        distanceContainer.appendChild(value);
})


//jsonp
// using jsonp request to extract data
function jsonpRequest(url, data) {
    // Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data) {
        if (data.hasOwnProperty(key)) {
            if (params.length == 0) {
                // First parameter starts with '?'
                params += "?";
            } else {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + "=" + encodedValue;
        }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}

let data = {
    callback: "getPortInfo"
};

jsonpRequest("https://eng1003.monash/api/v1/ports/", data);

let ports = [];

function getPortInfo(allPortData) {
    for (let i = 0; i < allPortData.ports.length; i++) {
        let name = allPortData.ports[i].name;
        let country = allPortData.ports[i].country;
        let type = allPortData.ports[i].type;
        let size = allPortData.ports[i].size;
        let precision =allPortData.ports[i].locprecision;
        let latitude = allPortData.ports[i].lat;
        let longitude = allPortData.ports[i].lng;

        let port = new Port(name, country, type, size ,precision , latitude, longitude);
        ports.push(port)

        //starrting point reference div

    }
    displayData();
}

function displayData() {

    let startDiv = document.getElementById("departure")
    let endDiv = document.getElementById("destination")
    let output = "";

    for (let j = 0; j < ports.length; j++) {

        output += '<option value="' + ports[j].country + ": " + ports[j].name + '">'
    }
    startDiv.innerHTML = output;
    endDiv.innerHTML = output;
}

markers_array = []

// now add some markers
function addMarker(coordinates) {
    let marker = new mapboxgl.Marker({
        "color": "#FF8C00"
    });
    marker.setLngLat(coordinates);
    // Display the marker.
    var a = marker.addTo(map)
    markers_array.push(a)
}


function reassemble_linestring() {

    // reassemble the linestring
    var linestring1 = {
        "type": "Feature",
        "geometry": {
            "type": "LineString",
            "coordinates": []
        }
    };
    linestring1.geometry.coordinates = geojson.features.map(function(point) {
        return point.geometry.coordinates;
    });

    geojson.features.push(linestring1)
    map.getSource('geojson').setData(geojson);
    
    distanceContainer.innerHTML = '';
    
    var distanceValue = turf.length(linestring1)
    
    var value1 = document.createElement('pre');
    value1.textContent =  'Total distance: ' + turf.length(linestring1).toLocaleString() + 'km';
    distanceContainer.appendChild(value1);
    
    let jsonStringDistance = JSON.stringify(distanceValue);
    localStorage.setItem("route_distance", jsonStringDistance);
}


// handle adding of waypoints on map upon click
map.on('click', function(e) {
    // gives you coorindates of the location where the map is clicked
    let coordinates = [e.lngLat.lng, e.lngLat.lat];
    var point1 = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": coordinates
        }
    };

    // we'll replace the last item in features with the second to last item
    geojson.features[geojson.features.length - 1] = geojson.features[geojson.features.length - 2];

    // and then insert the point into the second to last item
    geojson.features[geojson.features.length - 2] = point1


    reassemble_linestring();

    // add a marker as well
    new mapboxgl.Popup()
        .setLngLat(coordinates)
        //.setHTML(description) // add description to the popup
        .addTo(map);
    addMarker(coordinates)
});

// handle deleting of route waypoints like "undo" action
document.onkeydown = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 8) {
        if (geojson.features.length < 4) {
            geojson.features.pop()
            geojson.features.pop()
            geojson.features.pop()
            map.getSource('geojson').setData(geojson);
            return
        }

        // we'll remove the last item in features which is the linestring
        geojson.features.pop()

        // replace the 2nd to last item with last point item
        geojson.features[geojson.features.length - 2] = geojson.features[geojson.features.length - 1]
        geojson.features.pop()

        reassemble_linestring();

        var a = markers_array.pop()
        a.remove()
    }
};

let route=[];
let cost = 0;
function viewRoute() {
    let startDiv = document.getElementById("deptxt").value.split(":")
    let endDiv = document.getElementById("desttxt").value.split(":")
    let start = -1,
        dest = -1
    for (let i = 0; i < ports.length; i++) {
        if (ports[i].country === startDiv[0] &&
            startDiv[1].trim() === ports[i]._name) {
            start = i;
            console.log("start", start)
        }
        if (ports[i].country === endDiv[0] &&
            endDiv[1].trim() === ports[i]._name) {
            dest = i;
            console.log("dest", dest)
        }
    }
    if (start == dest) {
        alert("The source and destination cannot be the same")
        return
    }

    var point = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                ports[start].longitude,
                ports[start].latitude
            ]
        },
        "properties": {
            "id": String(new Date().getTime())
        }
    };
    geojson.features.push(point);
    var point = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                ports[dest].longitude,
                ports[dest].latitude
            ]
        },
        "properties": {
            "id": String(new Date().getTime())
        }
    };
    geojson.features.push(point);

    linestring.geometry.coordinates = geojson.features.map(function(point) {
        return point.geometry.coordinates;
    });
    geojson.features.push(linestring);
    map.getSource('geojson').setData(geojson);
    
    distanceContainer.innerHTML = '';
    
    distanceValue = turf.length(linestring)
    
    var value2 = document.createElement('pre');
    value2.textContent =  'Total distance: ' + turf.length(linestring).toLocaleString() + 'km';
    distanceContainer.appendChild(value2);

    // now add some markers
    let startLocation = []
    let destLocation = []
    
    function addMarkerStart(index) {
        let marker = new mapboxgl.Marker({
            "color": "#00FF00"
        });
        startLocation = [ports[index].longitude, ports[index].latitude];

        marker.setLngLat([ports[index].longitude, ports[index].latitude]);
        let popup = new mapboxgl.Popup({
            offset: 45
        });
        popup.setText(ports[index].name);
        marker.setPopup(popup)
        // Display the marker.
        marker.addTo(map);
        // Display the popup.
        popup.addTo(map);
    }
    
    function addMarkerDestination(index) {
        let marker = new mapboxgl.Marker({
            "color": "#FF0000"
        });
        destLocation = [ports[index].longitude, ports[index].latitude];

        marker.setLngLat([ports[index].longitude, ports[index].latitude]);
        let popup = new mapboxgl.Popup({
            offset: 45
        });
        popup.setText(ports[index].name);
        marker.setPopup(popup)
        // Display the marker.
        marker.addTo(map);
        // Display the popup.
        popup.addTo(map);
    }
    
    //Adding markers to destination and source port
    addMarkerStart(dest)
    addMarkerDestination(start)


    // save the startPort and destPort info to local storage
    // convert object into a string
    let jsonString = JSON.stringify(ports[start]);
    // put it in local storage
    localStorage.setItem("start_port", jsonString);
    jsonString = JSON.stringify(ports[dest]);
    // put it in local storage
    localStorage.setItem("dest_port", jsonString);

    let setDate = document.getElementById("date").value
    let jsonString2 = JSON.stringify(setDate);
    // put it in local storage
    localStorage.setItem("date_Of_Departure", jsonString2);
    
    // save the distance and cost

    let jsonStringDistance = JSON.stringify(distanceValue);
    localStorage.setItem("route_distance", jsonStringDistance);

    // now populate the div with information about the start and dest port
    let div = document.getElementById("dispJourney")
    div.innerHTML = "<h3><b>Starting Port</b></h3>" + "<br>" +
        "Start port: " + ports[start].name + "<br>" +
        "Start port type: " + ports[start].type + "<br>" +
        "Start port size: " + ports[start].size + "<br>" + "<br>" +
        "<h3><b>Destination Port</b></h3>" + "<br>" +
        "Destination port: " + ports[dest].name + "<br>" +
        "Destination port type: " + ports[dest].type + "<br>" +
        "Destination port size: " + ports[dest].size

    //map.panTo(depLocation)
    choosenShip = JSON.parse(localStorage.getItem("choosenShip"))
    let distance = lonlat_distance(ports[start]._longitude,ports[start]._latitude,ports[dest]._longitude,ports[dest]._latitude)
    let kph_speed = choosenShip._maxSpeed * 1.852
    // in units of hours
    let time_ship_travel = distance / kph_speed

    let saveRoute = new Route(
        choosenShip._shipName,
        ports[start]._name,
        ports[dest]._name,
        lonlat_distance(parseFloat(ports[start]._longitude),parseFloat(ports[start]._latitude),parseFloat(ports[dest]._longitude),parseFloat(ports[dest]._latitude)),
        time_ship_travel,
        cost,
        jsonString2)
    route.push(saveRoute)

    let routeSaved = JSON.parse(localStorage.getItem("routeSaved"))
    if (routeSaved == null){
        routeSaved = [];
    }
    let saveRouteUpdate = route.concat(routeSaved)
    localStorage.setItem("routeSaved", JSON.stringify(saveRouteUpdate));
    alert("New Route Added!")
}

function lonlat_distance(lon1, lat1, lon2, lat2) {
    var R = 6371e3; // metres
    let t1 = toRadians(lat1);
    let t2 = toRadians(lat2);
    let dp = toRadians(lat2 - lat1);
    let dl = toRadians(lon2 - lon1);
    let a = Math.sin(dp / 2) * Math.sin(dp / 2) +
        Math.cos(t1) * Math.cos(t2) *
        Math.sin(dl / 2) * Math.sin(dl / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let distance = (R * c / 1000).toFixed();
    cost = parseFloat(choosenShip._cost) * distance;
    return distance;

}

function toRadians(degrees) {
    let pi = Math.PI;
    return degrees * (pi / 180);
}
//calculating the duration for the journey
function hours_add(d, hr_add) {
    var d7 = new Date(d);
    let a = hr_add.toFixed()
    d7.setHours(d.getHours() + Number(a));
    return d7


}