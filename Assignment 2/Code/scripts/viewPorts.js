
let portArray = [];
let totalPortArray = [];

function jsonpRequest(url, data)
{
    // Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if (params.length == 0)
            {
                // First parameter starts with '?'
                params += "?";
            }
            else
            {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + "=" + encodedValue;
         }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}

// function to call the API for ports
function getPorts()
{
  let url = "https://eng1003.monash/api/v1/ports/"
  let data = {
    callback:"accessPort"
  };
  jsonpRequest(url, data);
}

// function to push the data from the API into portArray
function accessPort(apiData)
{
  for(i=0;i<apiData.ports.length;i++)
  {
    pName = apiData.ports[i].name;
    pCountry = apiData.ports[i].country;
    pType = apiData.ports[i].type;
    pSize = apiData.ports[i].size;
    pPrecision = apiData.ports[i].locprecision;
    pLat = apiData.ports[i].lat;
    pLong = apiData.ports[i].lng;

    let apiPort = new Port(pName,pCountry,pType,pSize,pPrecision,pLat,pLong)
    portArray.push(apiPort);
  }
  return portArray
}

// function to print a list of ports
function printPortList() {
    if (localStorage.getItem('userPort') == null){
    totalPortArray = portArray;
    }
    else {
    let userPortArray = JSON.parse(localStorage.getItem('userPort'));
    totalPortArray=userPortArray.concat(portArray);
    }

    var Porttable = document.getElementById("Port_table");

	for (let i = 0; i < totalPortArray.length; i++) {
    var row = Porttable.insertRow(i + 1);
    row.insertCell(0).innerHTML = totalPortArray[i]._name;
    row.insertCell(1).innerHTML = totalPortArray[i]._country;
    row.insertCell(2).innerHTML = totalPortArray[i]._type;
    row.insertCell(3).innerHTML = totalPortArray[i]._size;
    row.insertCell(4).innerHTML = totalPortArray[i]._precision;
    row.insertCell(5).innerHTML = "(" + totalPortArray[i]._latitude + "," + totalPortArray[i]._longitude + ")"
}

}
