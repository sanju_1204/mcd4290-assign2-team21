// Function for validation of Port input
function validatePort() {
    let name = document.getElementById('portName').value;
    let Country = document.getElementById('portCountry').value;
    let Type = document.getElementById('portType').value;
    let Size = document.getElementById('portSize').value;
    let Precision = document.getElementById('portPrecision').value;
    let Location = document.getElementById('portLocation').value;
    
    if (!validText(name)) {
        document.getElementById("portName").setAttribute("required",true);

    } else if (!validText(Country)) {
        document.getElementById("portCountry").setAttribute("required",true);

    } else if (!validText(Type)) {
        document.getElementById("portType").setAttribute("required",true);
    } else if (!validText(Size)) {
        document.getElementById("portSize").setAttribute("required",true);

    } else if (!validText(Precision)) {
        document.getElementById("portPrecision").setAttribute("required",true);

    } else if (!validText(Location)) {
        document.getElementById("portLocation").setAttribute("required",true);

    } else {
        getLocation();
    }
}

// Validate Text
function validText(text) {
    return text.length > 2;
}


let userPortArray = [];

// function for getting location from opencage API
function getLocation() {
    let nameSearch = document.getElementById("portLocation").value;
    
    opencage
    .geocode({q: nameSearch, key: '4ddb59bf62c6470c9075f5bc2b6ae58a', language:'en'})
    .then(data => {
        
        localStorage.setItem('portLatitude', data.results[0].geometry.lat);
        localStorage.setItem('portLongitude', data.results[0].geometry.lng);
    })
    
    .catch(error => {
        console.log('error', error.message);
    });
    setTimeout(function() { addNewPort(); }, 2000);
}

// function to add a new port
function addNewPort(){
    
    let portNew = new Port(
        document.getElementById('portName').value,
        document.getElementById("portCountry").value,
        document.getElementById("portType").value,
        document.getElementById("portSize").value,
        document.getElementById('portPrecision').value,
        localStorage.getItem('portLatitude'),
        localStorage.getItem('portLongitude')
        )
        
        localStorage.removeItem('portLongitude');
        localStorage.removeItem('portLatitude');
        
        userPortArray.push(portNew);
        storePorts1(userPortArray);
    }
    
    getPorts();
    
    // local Storage for ports
    const PORT_STORE_KEY1 = "userPort";
    
    function storePorts1(portList)
    {
        let portSaved = JSON.parse(localStorage.getItem(PORT_STORE_KEY1));
if (portSaved==null || portSaved==undefined ){
    portSaved = [];
}

let newPortList = portList.concat(portSaved);
localStorage.setItem(PORT_STORE_KEY1, JSON.stringify(newPortList));
alert("New port added!")

// After the excetution open the viewPort webpage
window.location.replace("viewPorts.html");
}





