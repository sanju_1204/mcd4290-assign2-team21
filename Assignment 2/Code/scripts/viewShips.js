
// empty array for ship

let shipArray=[];
let totalShipArray = [];

function jsonpRequest(url, data)
{
    // Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if (params.length == 0)
            {
                // First parameter starts with '?'
                params += "?";
            }
            else
            {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + "=" + encodedValue;
         }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}

// function to call the API for ships
function getShips()
{
  let url = "https://eng1003.monash/api/v1/ships/"
  let data = {
    callback:"accessShip"
  };
  jsonpRequest(url, data);
}


// function to push the data from the API into shipArray
function accessShip(apiData)
{
  for(i=0;i<apiData.ships.length;i++)
  {
    sName=apiData.ships[i].name;
    sMaxSpeed=apiData.ships[i].maxSpeed;
    sRange=apiData.ships[i].range;
    sDesc=apiData.ships[i].desc;
    sCost=apiData.ships[i].cost;
    sStatus=apiData.ships[i].status;
    sComments=apiData.ships[i].comments;

    let apiShip = new Ship(sName,sMaxSpeed,sRange,sDesc,sCost,sStatus,sComments)
    shipArray.push(apiShip);
    
    
  }
  return shipArray;
}

// function to generate a list of ships
function printShipList() {

  if (localStorage.getItem('userShip') == null){
  totalShipArray = shipArray;
  }
  else {
  userShipArray = JSON.parse(localStorage.getItem('userShip'));
  totalShipArray=userShipArray.concat(shipArray);
  }

  var Shiptable = document.getElementById("Ship_table");
  
  for (let i = 0; i < totalShipArray.length; i++) {
    var row = Shiptable.insertRow(i + 1);
    row.insertCell(0).innerHTML = totalShipArray[i]._shipName;
    row.insertCell(1).innerHTML = totalShipArray[i]._desc;
    row.insertCell(2).innerHTML = totalShipArray[i]._range;
    row.insertCell(3).innerHTML = totalShipArray[i]._cost;
    row.insertCell(4).innerHTML = totalShipArray[i]._maxSpeed;
    row.insertCell(5).innerHTML = totalShipArray[i]._status;
    row.insertCell(6).innerHTML = "<td><a class=\"mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect\" onclick=\"selectShipButton("+i+")\">Select</a></td>";
  }
}



// function to find the index of each ship in the array
function chooseShip(index)
{
    if (index >= totalShipArray.length)
    {
      return null;
    }
    console.log(totalShipArray[index])
    return totalShipArray[index];    

}

// This function is used to show the details of the ship when the 'view' button is clicked
function viewShipButton(index)
{
  document.getElementById("shipInfo").style.visibility="visible";
  let choosenShip = chooseShip(index)
  document.getElementById("shipName").innerText = choosenShip._shipName;
  document.getElementById("shipSpeed").innerText = choosenShip._maxSpeed;
  document.getElementById("shipRange").innerText = choosenShip._range;
  document.getElementById("shipDesc").innerText = choosenShip._desc;
  document.getElementById("shipCost").innerText = choosenShip._cost;
  document.getElementById("shipStatus").innerText = choosenShip._status;
  
}

function selectShipButton(index)
{
  let choosenShip = chooseShip(index)
  if (choosenShip==null)
  {
    alert("Please select ship from shi list")
  }
  let jsonString = JSON.stringify(choosenShip);
  // put it in local storage
  localStorage.setItem("choosenShip",jsonString);
    
    window.location.replace("addRoute.html")
}



