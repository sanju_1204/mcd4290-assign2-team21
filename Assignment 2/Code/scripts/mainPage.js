// API

let scriptShips = document.createElement('script')
scriptShips.src= "https://eng1003.monash/api/v1/ships/?callback=viewShips";
document.body.appendChild(scriptShips);

let scriptPorts = document.createElement('script')
scriptPorts.src= "https://eng1003.monash/api/v1/ports/?callback=viewPorts";
document.body.appendChild(scriptPorts); 


function viewShips (shipsAPI) 
{
console.log(shipsAPI.ships)
localStorage.setItem("shipsAPI", JSON.stringify(shipsAPI))


}

function viewPorts (portsAPI)

{
console.log(portsAPI.ports)
localStorage.setItem("portsAPI", JSON.stringify(portsAPI))
}

// view routes
function viewRoutes()
{ let newRoute = JSON.parse(localStorage.getItem("routeSaved"))
console.log(newRoute);
newRoute.sort(function(a, b) {
    return a._cost - b._cost;
});
console.log(newRoute);
 

var Routetable = document.getElementById("viewRoute");

for (let i = 0; i < newRoute.length; i++) {
  var row = Routetable.insertRow(i + 1);
  row.insertCell(0).innerHTML = newRoute[i]._sourcePort;
  row.insertCell(1).innerHTML = newRoute[i]._cost;
  row.insertCell(2).innerHTML = newRoute[i]._distance;
  row.insertCell(3).innerHTML = (newRoute[i]._time/24).toFixed(2);
  row.insertCell(4).innerHTML = newRoute[i]._destinationPort;
  row.insertCell(5).innerHTML = "<td><a class=\"mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect\" href=\"viewRoute.html\" onclick=\"viewRouteDetail("+i+")\">View</a></td>";

}
 
function DeleteRouteView(){
    newRoute.pop()
}

}

function viewRouteDetail(i) {
let option = JSON.parse(localStorage.getItem("routeSaved"));
let chosen = option[i];
localStorage.setItem("routeChosed",JSON.stringify(chosen));

}